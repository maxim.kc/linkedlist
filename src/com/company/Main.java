package com.company;

import com.company.linkedlist.LinkedList;

public class Main {

    public static void main(String[] args) {
        // write your code here
        LinkedList<String> ll = new LinkedList<>();
        ll.addLast("B");
        ll.addLast("C");
        ll.addLast("D");
        ll.addLast("E");
        System.out.println(ll);
        ll.addFirst("A");
        System.out.println(ll);
        System.out.println(ll.getValue(0));
        System.out.println(ll.getSize());
        ll.removeLast();
        System.out.println(ll.getSize());
        System.out.println(ll);
        ll.remove("D");
        System.out.println(ll);
        System.out.println(ll.getSize());
        ll.removeFirst();
        System.out.println(ll.getIndex("C"));
        ll.reverse();
        System.out.println(ll);
        System.out.println(ll.get("E"));
        String[] s = new String[]{"W", "Y"};
        ll.fromArray(s);
        System.out.println(ll);


    }
}
