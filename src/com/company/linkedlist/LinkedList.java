package com.company.linkedlist;


import java.lang.reflect.Array;
import java.util.Arrays;

public class LinkedList<E>{
    private Node<E> head;
    int size = 0;

    public void addFirst(E data) {
        Node<E> firstNode = new Node<>(data);
        firstNode.next = head;
        head = firstNode;
        size++;
    }

    public void addLast(E data) {
        if(head == null) {
            head = new Node<>(data);
        }
        else {
            Node<E> currentNode = head;
            while (currentNode.next != null) {
                currentNode = currentNode.next;
            }
            currentNode.next = new Node<>(data);
        }
        size++;
    }

    public E getValue(int index){
        int counter = 0;
        Node<E> currentNode = head;
        while (currentNode != null) {
            if (counter == index){
                return currentNode.value;
            }
            currentNode = currentNode.next;
            counter++;
        }
        return null;
    }

    public int getIndex(E data) {
        if (head == null) {
            return -1;
        }

        Node<E> currentNode = head;
        int index = 0;
        while (currentNode != null) {
            if(currentNode.value == data) {
                return index;
            }
            index++;
            currentNode = currentNode.next;
        }
        return -1;
    }

    public Node<E> get(E data){
        if (head == null) {
            return null;
        }
        Node<E> currentNode = head;
        while (currentNode != null) {
            if(currentNode.value == data) {
                return currentNode;
            }
            currentNode = currentNode.next;
        }
        return null;
    }

    public void remove(E data) {
        if(head == null) {
            return;
        }
        if (head.value == data) {
            head = head.next;
            size--;
            return;
        }
        Node<E> currentNode = head;
        while (currentNode.next != null) {
            if(currentNode.next.value == data) {
                currentNode.next = currentNode.next.next;
                size--;
                return;
            }
            currentNode = currentNode.next;
        }
    }

    public void removeFirst(){
        if(head != null) {
            head = head.next;
            size--;
        }
    }

    public void removeLast() {
        if(head == null) {
            return;
        }
        if (head.next == null) {
            head = null;
            size--;
            return;
        }
        Node<E> currentNode = head;
        while (currentNode.next != null) {
            if(currentNode.next.next == null) {
                currentNode.next = null;
                size--;
                return;
            }
            currentNode = currentNode.next;
        }

    }

    public int getSize() {
        return size;
    }

    public Object[] toArray() {
        Object[] result = new Object[size];
        int i = 0;
        for (Node<E> x = head; x != null; x = x.next)
            result[i++] = x.value;
        return result;
    }

    public void fromArray(E[] array){

        for (E element: array) {
            addLast(element);
        }
    }


    public void reverse(){
        Node<E> currentNode = head;
        Node<E> previousNode = null;
        Node<E> nextNode;

        while (currentNode != null) {
            nextNode = currentNode.next;
            currentNode.next = previousNode;
            previousNode = currentNode;
            currentNode = nextNode;
        }
        head = previousNode;
    }

    @Override
    public String toString() {
        return "LinkedList{" +
                "head=" + head +
                '}';
    }

}
